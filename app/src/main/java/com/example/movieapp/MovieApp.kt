package com.example.movieapp

import android.app.Application
import com.example.movieapp.data.db.AppDatabase

class MovieApp : Application() {
    override fun onCreate() {
        super.onCreate()
        AppDatabase.getInstance(this)
    }
}