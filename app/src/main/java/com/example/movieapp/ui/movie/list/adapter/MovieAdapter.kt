package com.example.movieapp.ui.movie.list.adapter

import android.util.Log
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.domain.entity.Movie
import com.example.movieapp.ui.base.BaseViewHolder

class MovieAdapter(private var listener: Listener): RecyclerView.Adapter<BaseViewHolder<Movie>>(){

    private var items = mutableListOf<Movie>()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): BaseViewHolder<Movie> {
        return MovieViewHolder.create(parent, listener)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Movie>, position: Int) {
        holder.onBind(items[position])
    }

    override fun getItemCount() = items.count()

    fun addMovies(movies: MutableList<Movie>){
        items.clear()
        items.addAll(movies)
        notifyDataSetChanged()
    }

    interface Listener{
        fun onMovieClick(movie: Movie)
    }
}