package com.example.movieapp.ui.auth.login

import android.util.Patterns
import androidx.lifecycle.ViewModel

class LoginViewModel : ViewModel() {

    fun validateInputs(login : String?, password: String?) : Boolean = validateEmail(login) && validatePassword(password)
    fun validatePassword(password : String?) : Boolean = !(password == null || password!!.isEmpty())
    fun validateEmail(email : String?) : Boolean = !(email == null || email.isEmpty()) && Patterns.EMAIL_ADDRESS.matcher(email).matches()

}