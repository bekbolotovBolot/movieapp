package com.example.movieapp.ui.movie.list.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.movieapp.R
import com.example.movieapp.domain.entity.Movie
import com.example.movieapp.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.fragment_movie_details.view.*
import kotlinx.android.synthetic.main.movie_card.view.*

class MovieViewHolder(itemView: View) : BaseViewHolder<Movie>(itemView) {

    private lateinit var movie: Movie
    override fun onBind(item: Movie) {
        movie = item
        itemView.tv_movie_title.text = movie.title
    }

    companion object {

        fun create(parent: ViewGroup, listener: MovieAdapter.Listener): MovieViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.movie_card, parent, false)
            val holder = MovieViewHolder(view)
            holder.itemView.setOnClickListener { listener.onMovieClick(holder.movie) }
            return holder
        }
    }
}