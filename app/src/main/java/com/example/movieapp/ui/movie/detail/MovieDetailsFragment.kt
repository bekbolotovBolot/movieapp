package com.example.movieapp.ui.movie.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.movieapp.domain.entity.Movie
import kotlinx.android.synthetic.main.fragment_movie_details.*
import org.parceler.Parcels


class MovieDetailsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(com.example.movieapp.R.layout.fragment_movie_details, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupDetails()
    }
    private fun setupDetails() {
        arguments?.let{ arg ->
            val movie: Movie = Parcels.unwrap(arg.getParcelable(Movie::class.java.canonicalName))
            tv_movie_name.text = movie.title
            tv_movie_description.text = movie.description
            movie.rating?.let { rb_movie_rating.rating = it }
        }
    }
}
