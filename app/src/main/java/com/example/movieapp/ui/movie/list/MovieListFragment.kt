package com.example.movieapp.ui.movie.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.movieapp.MovieApp
import com.example.movieapp.data.db.AppDatabase
import com.example.movieapp.domain.entity.Genre
import com.example.movieapp.domain.entity.Movie
import com.example.movieapp.ui.movie.MovieActivity
import com.example.movieapp.ui.movie.MovieViewModel
import com.example.movieapp.ui.movie.list.adapter.MovieAdapter
import kotlinx.android.synthetic.main.fragment_movie_list.*

class MovieListFragment : Fragment(), MovieAdapter.Listener {

    private var movieList = mutableListOf<Movie>()
    private lateinit var adapter: MovieAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var list = ArrayList<Genre>()
        movieList.add(Movie(title = "Movie1", rating = 5.5f, description = "sdfsasdf"))
        movieList.add(Movie(title = "Movie2", rating = 10f, description = "Hsdfd"))
        movieList.add(Movie(title = "Movie3", rating = 8.2f, description = "I am your father"))
        movieList.add(Movie(title = "Movie4", rating = 1.8f, description = "twilidsgsdft"))
        movieList.add(Movie(title = "Movie5", rating = 4.7f, description = "Avengsdfsdle"))
        movieList.add(Movie(title = "Movie6", rating = 7f, description = "my darling"))
        movieList.add(Movie(title = "Movie7", rating = 6f, description = "dsfsdaf"))
        movieList.add(Movie(title = "Movie8", rating = 4.2f, description = "Anssdffe"))
        movieList.add(Movie(title = "Movie9", rating = 2.7f, description = "asdf"))

        AppDatabase.getInstance(activity!!.applicationContext).getMovieDao().insert(movieList[0])
        AppDatabase.getInstance(activity!!.applicationContext).getMovieDao().insert(movieList[1])
        AppDatabase.getInstance(activity!!.applicationContext).getMovieDao().insert(movieList[2])
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(com.example.movieapp.R.layout.fragment_movie_list, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycleView()
    }

    private fun setupRecycleView(){
        rv_movies.layoutManager = LinearLayoutManager(context)
        rv_movies.itemAnimator = DefaultItemAnimator()
        adapter = MovieAdapter(this)
        rv_movies.adapter = adapter
        adapter.addMovies(movieList)

    }

    override fun onMovieClick(movie: Movie) {
        (activity as MovieActivity).openMovieDetailsFragment(movie)
    }
}
