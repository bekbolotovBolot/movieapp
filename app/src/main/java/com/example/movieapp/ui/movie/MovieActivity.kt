package com.example.movieapp.ui.movie
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.movieapp.R
import com.example.movieapp.domain.entity.Movie
import com.example.movieapp.ui.movie.detail.MovieDetailsFragment
import com.example.movieapp.ui.movie.list.MovieListFragment
import org.parceler.Parcels


class MovieActivity : AppCompatActivity() {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie)
        openMovieListFragment()

    }
    fun openMovieDetailsFragment(movie : Movie) {
        val movieDetails = MovieDetailsFragment()
        val bundle = Bundle()
        bundle.putParcelable(Movie::class.java.canonicalName, Parcels.wrap(movie))
        movieDetails.arguments = bundle
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, movieDetails)
                                                 .addToBackStack(null).commit()
    }
    private fun openMovieListFragment() {
        supportFragmentManager.beginTransaction().add(R.id.fragment_container, MovieListFragment()).commit()
    }
    companion object {
        fun start(context: Context): Intent {
            return Intent(context, MovieActivity::class.java)
        }
    }
}
