package com.example.movieapp.ui.auth.login

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.example.movieapp.R
import com.example.movieapp.ui.movie.MovieActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    private lateinit var viewModel: LoginViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        setupViews()
    }

    private fun setupViews() {
        login_button.setOnClickListener {
            if (viewModel.validateInputs(et_login.text.toString(), et_password.text.toString()))
                startActivity(MovieActivity.start(this))
            else {
                if (!viewModel.validatePassword(et_password.text.toString()))
                    layout_password.error = "Password should not be empty"
                if(!viewModel.validateEmail(et_login.text.toString()))
                    layout_login.error = "Invalid email"
            }
        }

        et_login.addTextChangedListener(WatcherText {layout_login.error = null})
        et_password.addTextChangedListener(WatcherText { layout_login.error = null})
    }

    inner class WatcherText(val onTextChanged:() -> Unit) : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            onTextChanged()
        }
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

    }
}
