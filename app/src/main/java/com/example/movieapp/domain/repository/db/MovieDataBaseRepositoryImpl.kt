package com.example.movieapp.domain.repository.db

import com.example.movieapp.data.db.MoviesDao
import com.example.movieapp.domain.entity.Movie


class MovieDataBaseRepositoryImpl(val dao: MoviesDao) : DataBaseMovieRepository {

    override fun getAll() = dao.getAll()

    override fun getDetails(id: Int) = dao.getById(id)


    override fun update(movie: Movie) {
        dao.update(movie)
    }

    override fun deleteById(id: Int) {
        dao.deleteById(id)
    }
    override fun deleteAll() {
        dao.deleteAll()
    }

    override fun insertAll(list: List<Movie>) {
        dao.insertAll(list)
    }

}