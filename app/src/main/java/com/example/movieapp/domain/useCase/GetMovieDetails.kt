package com.example.movieapp.domain.useCase

import com.example.movieapp.domain.entity.Movie
import com.example.movieapp.domain.repository.db.DataBaseMovieRepository

class GetMovieDetails(var repository: DataBaseMovieRepository) : UseCase<GetMovieDetails.Params, Movie>{
    override fun invoke(params: Params): Movie = repository.getDetails(params.id)
    data class Params(var id: Int)
}