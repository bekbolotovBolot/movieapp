package com.example.movieapp.domain.useCase

interface UseCase<T, S> {
    fun invoke(params: T) : S
}