package com.example.movieapp.domain.repository

import com.example.movieapp.domain.entity.Movie

interface MovieRepository {
    fun getAll() : List<Movie>
    fun getDetails(id : Int) : Movie

}