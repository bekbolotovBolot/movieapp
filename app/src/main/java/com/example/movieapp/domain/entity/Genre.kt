package com.example.movieapp.domain.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.parceler.Generated
import org.parceler.Parcel

@Parcel
@Entity(tableName = "genre")
data class Genre(@PrimaryKey(autoGenerate = true) var id : Int? = null,
                             var title: String? = null)