package com.example.movieapp.domain.repository.db

import com.example.movieapp.domain.entity.Movie
import com.example.movieapp.domain.repository.MovieRepository


interface DataBaseMovieRepository : MovieRepository{
    fun update(movie: Movie)
    fun deleteById(id: Int)
    fun insertAll(list : List<Movie>)
    fun deleteAll()
}