package com.example.movieapp.domain.entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import org.parceler.Parcel

@Parcel
@Entity(tableName = "person")
data class Person(@PrimaryKey(autoGenerate = true) var id : Int? = null,
                              var fullName : String? = null)