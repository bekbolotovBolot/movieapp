package com.example.movieapp.domain.useCase

import com.example.movieapp.domain.entity.Movie
import com.example.movieapp.domain.repository.db.DataBaseMovieRepository

class GetListOfMovies(var repository: DataBaseMovieRepository) : UseCase<Any, List<Movie>> {
    override fun invoke(params: Any): List<Movie> = repository.getAll()
}