package com.example.movieapp.domain.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.parceler.Parcel

@Parcel
@Entity(tableName = "movie")
data class Movie(@PrimaryKey(autoGenerate = true) var id: Int? = null,
                 var title: String? = null,
                 var rating: Float? = null,
                 var genres: List<Genre>? = null,
                 var description: String? = null,
                 var director: Person? = null,
                 var cast: List<Person>? = null)