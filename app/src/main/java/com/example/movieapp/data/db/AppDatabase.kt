package com.example.movieapp.data.db

import android.content.Context
import androidx.room.*
import com.example.movieapp.domain.entity.Genre
import com.example.movieapp.domain.entity.Movie
import com.example.movieapp.domain.entity.Person

@Database(entities = [Movie::class, Genre::class, Person::class], version = 1)
@TypeConverters(TypeConverter::class)
abstract class AppDatabase : RoomDatabase(){
    abstract fun getMovieDao() : MoviesDao

    companion object {
        private var instance : AppDatabase? = null
        private var DB_NAME = "movieapp.db"
        fun getInstance(context: Context) : AppDatabase {
            if (instance == null) {
                instance = Room.databaseBuilder(context, AppDatabase::class.java, DB_NAME)
                        .allowMainThreadQueries()
                        .build()
            }
            return instance!!
        }
    }
}