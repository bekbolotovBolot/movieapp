package com.example.movieapp.data.db

import androidx.room.TypeConverter
import com.example.movieapp.domain.entity.Genre
import com.example.movieapp.domain.entity.Person
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class TypeConverter {
    @TypeConverter
    fun fromGenreToJSON(value : Genre?) = Gson().toJson(value)

    @TypeConverter
    fun fromJSONToGenre(value: String?)  = Gson().fromJson(value, Genre::class.java)

    @TypeConverter
    fun fromPersonToJSON(value: Person?) = Gson().toJson(value)

    @TypeConverter
    fun fromJSONToPerson(value: String?)  = Gson().fromJson(value, Person::class.java)

    @TypeConverter
    fun fromGenreListToJSON(value: List<Genre>?) = Gson().toJson(value)

    @TypeConverter
    fun fromJSONToGenreList(value: String?) : List<Genre>? = Gson().fromJson(value, object : TypeToken<List<Genre>>(){}.type)

    @TypeConverter
    fun fromPersonListToJSON(value: List<Person>?) = Gson().toJson(value)

    @TypeConverter
    fun fromJSONToPersonList(value: String?) : List<Person>? = Gson().fromJson(value, object : TypeToken<List<Person>>(){}.type)
}