package com.example.movieapp.data.db

import androidx.room.*
import com.example.movieapp.domain.entity.Movie

@Dao
interface MoviesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movie: Movie)

    @Insert
    fun insertAll(list : List<Movie>)

    @Update
    fun update(movie: Movie)

    @Query("DELETE FROM movie")
    fun deleteAll()

    @Query("DELETE FROM movie WHERE id == :id")
    fun deleteById(id: Int)

    @Query("SELECT * FROM movie")
    fun getAll(): List<Movie>

    @Query("SELECT * FROM movie WHERE id == :id")
    fun getById(id: Int): Movie
}